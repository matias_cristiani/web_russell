<?php require('autenticar.php') ;?>
<html lang="es">
  
  <?php include 'head.php';?>

  <body class="text-center">
	
	<div class="container offline d-none">
		<header class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
    			<div class="alert alert-warning  none">Se require conexión a internet</div>
    		</div>
    	</header>
    </div>

    <div class="container container-flex">
        <div class="row ">
          <div class="col-md-12">
            <div class="loader"></div>
          </div>
        </div>
        <div class="row ">
          <div class="col-md-12">
              <p>Cerrando...</p>
          </div>
        </div>

        <hr>

        <footer>
          <p class="mt-5 mb-3 text-muted">&copy; Colegio Bertrand Russell <?php echo date("Y") ?></p>
        </footer>

      </div>


    <?php include 'footer.php';?>

    
<script type="text/javascript">
$(function() {

  try {
    appLogout();
  } catch(err) {
    oWebViewInterface.emit('listenClear');
  }

  window.location = 'https://www.colegiorussell.edu.ar/app/token_familia/';
  
});
</script>



</body>
</html>