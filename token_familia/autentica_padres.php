﻿<?php

//Connection statement
require_once('../../brpadres/Connections/BaseLocal.php');

//Aditional Functions
require_once('../../brpadres/includes/functions.inc.php');

$KT_LoginAction = $_SERVER["REQUEST_URI"];
$modoDemo = false;
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  if ($_POST["restablecer"])
  {
  	  $url = "../../comunes/php/restablecerContrasena.php?tipo=pad";
  	  if ($_POST['login_dni'])
  	  {
  	  	  $url .= "&dni=" . $_POST['login_dni'];
	  }

  	  KT_redir($url);
  } else if ($_POST["submit"] == "DEMO VISITANTES") {
  	$modoDemo = true;
    $KT_valUsername = "99999999";
	  $contrasena = "1234";
	  $KT_rsUser_Source = sprintf("SELECT NroDoc, LegFam FROM qryLogin WHERE NroDoc = %d", GetSQLValueString($KT_valUsername, "int"));
  }

  else
  {
  	$modoDemo = false;
	  $KT_valUsername = $_POST['login_dni'];
	  $contrasena = md5(trim($_POST['login_pass']));
	  $KT_rsUser_Source = sprintf("SELECT NroDoc, LegFam FROM qryLogin WHERE NroDoc = %d and contra = %s", GetSQLValueString($KT_valUsername, "int"), GetSQLValueString($contrasena, "text"));
  }

  $redireccionar_ = "https://www.colegiorussell.edu.ar/brpadres/datos_pad.php"; 

  $KT_rsUser = $BaseLocal->Execute($KT_rsUser_Source) or DIE("error al ejecutar qryLogin: ". $BaseLocal->ErrorMsg());

  // print_r($KT_rsUser);
  // exit();

  if (!$KT_rsUser->EOF) {
    $KT_rsUser->Close();
  	
    $legajoFamilia = $KT_rsUser->Fields('LegFam');
  	$_SESSION["legajoFamilia"] = $legajoFamilia;
  	$_SESSION["KT_Username"] = $KT_valUsername;
  	$_SESSION["contra"] = $contrasena;
  	$_SESSION["Demo"] = $modoDemo;
	
    $param = $_POST['login_dni']; 
    $param2 = $_POST['login_pass'];
    $param3 = $_POST['app_id']; 

    header("Location: ".$redireccionar_."?logindni=".urlencode($param)."&loginpass=".urlencode($param2)."&app_id=".urlencode($param3)); 
    exit();
} else {
	//Parametro fuera de sesion
	$_SESSION["login_app"] = "2";	
  	$mensaje = "DNI o Contraseña incorrectos, por favor intente nuevamente";
    $redireccionar = "https://www.colegiorussell.edu.ar/app/token_familia/index.php?mensaje=".$mensaje;
    header("Location: ".$redireccionar );

  }
}
?>