  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
    <!-- Custom styles for this template -->
    <link href="../token/style.css" rel="stylesheet">
	
	<!-- offline -->
    <link rel="stylesheet" href="../token/offline/offline-theme-chrome.css" />
    <link rel="stylesheet" href="../token/offline/offline-language-spanish.css" />
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	
    <title><?php echo($_SESSION['titulo']) ?></title>
  </head>