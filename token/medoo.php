<?php
class medoo {

	public function __construct($odbc_name){
		if (!$this->conn_access = odbc_connect ($odbc_name, "", "")){
			die("No se pudo conectar a ".$odbc_name);
		}
	}

	public function query($query){
		if(!$this->rs_access = odbc_exec ($this->conn_access, $query)){
			die("Error al ejecutar el query: ".$query);
		} else {
			$this->objeto = new medoo_Extend();
			$this->objeto->recurso($this->rs_access);
			return $this->objeto;
		}
	}
}

class medoo_Extend {

	public function recurso($rs_access){
		$this->rs_access = $rs_access;
	}

	public function fetchAll(){
		$resultado = array();
		while ($row = odbc_fetch_array($this->rs_access)){
			$resultado[] = $row;
		}
		return $resultado;
	}
}

?>