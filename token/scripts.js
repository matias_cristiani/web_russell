var oWebViewInterface = window.nsWebViewInterface;

(function(){
    var loginUser = 0;
    var loginPass = 0;

    var url_ = window.location.href;
    
    var url = new URL(url_)
    var dni_ = url.searchParams.get("logindni");
    var pass_ = url.searchParams.get("loginpass");
    
    if (dni_ && Number(dni_) > 0) {
      loginUser = dni_
      loginPass = pass_
    }
       
    /** 
     * Registers handlers: offline / online mode .
     */
    var $online = $('.online');
    var $offline = $('.offline');

    Offline.on('confirmed-down', function () {
                    
        $online.fadeOut(function () {
            $offline.fadeIn();
        });

        if ($('.btn')) {
            $('.btn').attr("disabled", "disabled");
        }

    });

    Offline.on('confirmed-up', function () {
                    
                    $offline.fadeOut(function () {
                        $online.fadeIn();
                    });

                    if ($('.btn')) {
                        $('.btn').removeAttr("disabled");
                    }
    });
    
    /**
     * Registers handlers for native events.
     */

    
    
    function addNativeMsgListener() {
        /*
        oWebViewInterface.on('loadLanguages', function (arrLanguages) {
            for (var i = 0; i < arrLanguages.length; i++) {
                addLanguageOption(arrLanguages[i]);
            }
        });
        */
    }
    
    /**
     * Defines global functions which will be called from android/ios
     */
    function defineNativeInterfaceFunctions(){

        
        window.iniciar_sesion = function(usuario){
            
            console.log(usuario);
            $('#inputDni').val(usuario.dni);
            $('#inputPassword').val(usuario.password);
            $('#f1').css('display', 'none');
            
            if (usuario.dni && usuario.password) {
			
               $('.form-signin').submit();    
            

            }

            setTimeout(function(){
                 $('#f1').css('display', 'block');
            }, 5000);

            return new Promise(function(resolve) {
                    resolve('OK');
                    // resolve(false);
            });
        };


         window.app_id = function(app_id){
            console.log('app_id');
            console.log(app_id);

              if (app_id && app_id == 'ar.edu.colegiorussell.v2.alumnos') {
                   $('#inputApp_id').val(app_id);

                    var dni_etiqueta = "D.N.I. ALUMNO";
                    $('label.labelDNI').text(dni_etiqueta);
                
                }else if(app_id && app_id == 'ar.edu.colegiorussell.v2.familia'){
                   $('#inputApp_id').val(app_id);

                    var dni_etiqueta = "D.N.I. FAMILIA";
                    $('label.labelDNI').text(dni_etiqueta);
                
                }else if(app_id && app_id == 'ar.edu.colegiorussell.v2.personal'){
                   $('#inputApp_id').val(app_id);

                    var dni_etiqueta = "D.N.I. PERSONAL";
                    $('label.labelDNI').text(dni_etiqueta);
                }else{
                    alert('APP_ID, Sin Declarar');
                }
                


                // window.localStorage.setItem('appid', app_id);
                setCookie("appid", app_id, 365);

            return new Promise(function(resolve) {
                    resolve('APP_ID SETEADO');
            });
        };
      
       window.app_notificacion = function(notificacion){

            var dni = notificacion.dni;
            var appid = getCookie("appid");
            var codigo = notificacion.codigo;

            console.log('notificacion:');
            console.log(notificacion.dni);
            console.log(notificacion.codigo);
            
            var url = 'https://www.colegiorussell.edu.ar/brprofes/edit_notificaciones_get.php?clave='+codigo+'&username='+dni+'&mostrarTextoMail=true&app=true';

             switch (appid) {
               
               case 'ar.edu.colegiorussell.v2.familia':
                url = 'https://www.colegiorussell.edu.ar/brpadres/edit_notificaciones_get.php?clave='+codigo+'&username='+dni+'&mostrarTextoMail=true&app=true';
                break;
                
                case 'ar.edu.colegiorussell.v2.alumnos':
                url = 'https://www.colegiorussell.edu.ar/bralumnos/edit_notificaciones_get.php?clave='+codigo+'&username='+dni+'&mostrarTextoMail=true&app=true';
              break;

                case 'ar.edu.colegiorussell.v2.personal':
                url = 'https://www.colegiorussell.edu.ar/brprofes/edit_notificaciones_get.php?clave='+codigo+'&username='+dni+'&mostrarTextoMail=true&app=true';
                // https://www.colegiorussell.edu.ar/brprofes/edit_notificaciones_get.php?clave=1550857101&username=27822657&mostrarTextoMail=true
              break;
            }

            console.log(url);

            if (codigo && dni) {
                console.log('OK')
                window.location.href = url;
            }

            return new Promise(function(resolve) {
                    resolve(url);
            });
        };

    }

    function sendUser(usuario){
        oWebViewInterface.emit('listenUser', usuario);
    }

    function appLogout(){
        oWebViewInterface.emit('listenClear');   
    }
    
     function sendDemo(usuario){
        oWebViewInterface.emit('anyEvent', 'usuario');
    }
    
    function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        }

    function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+ d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }


    /**
     * Defines init webview interface
     */
    function init() {
        
        defineNativeInterfaceFunctions();
        
        setTimeout(function(){
            
            //limitar-dni
            $("#inputDni").keyup(function() {
                var maxChars = 8;
                if ($(this).val().length > maxChars) {
                    $(this).val($(this).val().substr(0, maxChars));
                }
            });
            
            
            //mostrar-online
            $('#f1').css('opacity', 1);
            $('.online').css({ 'opacity': 1,  'transition': '1s' });
            

            if ( loginUser && loginUser > 0 ){
                
                console.log('WEB: user'+loginUser);
                console.log('WEB: pass'+loginPass);

                setTimeout(function(){
                    sendUser({dni: loginUser, password: loginPass});
                }, 1000);
            
                
            }else{
                console.log(loginUser);
            }

        }, 400);
    
        
        
    }







    /**
     * init webview
     */
    init();


     /**
     * Cerrar Sessión y des-registrar push
     */
    // $('#cerrarSesion').onclick( "click", function() {      
    
    $(document).on("click",".btn-register",function() {
      
      $.when( 
        alert('PARA COMPLETAR EL REGISTRO DEBERAS ACERCARTE AL COLEGIO BERTRAND RUSSELL QUE SE ENCUENTRA UBICADO EN Aráoz 350, B1823 Banfield, Bs As, CON EL OBJETIVO DE COMPLETAR EL REGISTRO Y CORROBORAR TU IDENTIDAD PARA QUE YA SEAS PARTE DE NUESTRO COLEGIO.')
        ).then(function( data, textStatus, jqXHR ) {
          window.location.href = 'https://www.colegiorussell.edu.ar/app/token_personal/';
      });
      
      
      
    }) 
    
    var boton = document.getElementById("cerrarSesion");
    
    if (boton) {
        boton.onclick = function(){
            // var appid = localStorage.getItem('appid');
            var appid = getCookie("appid");
            console.log('cerrando session: ',appid);

            
            if (appid && appid == 'ar.edu.colegiorussell.v2.alumnos') {
                $("#cerrarSesion").attr("href", "https://www.colegiorussell.edu.ar/app/token_alumnos/");
            }else if(appid && appid == 'ar.edu.colegiorussell.v2.familia'){
                $("#cerrarSesion").attr("href", "https://www.colegiorussell.edu.ar/app/token_familia/");
            }else if(appid && appid == 'ar.edu.colegiorussell.v2.personal'){
                $("#cerrarSesion").attr("href", "https://www.colegiorussell.edu.ar/app/token_personal/");
            }
            
            appLogout();
        };
    }

})();