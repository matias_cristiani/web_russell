<!--js-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- Optional JavaScript -->
    <script type="text/javascript" src="../token/lib/es6-promise.min.js"></script>
    <script type="text/javascript" src="../token/lib/nativescript-webview-interface.js"></script>
	  <script type="text/javascript" src="../token/offline/offline.min.js"></script>
	
   
    <script type="text/javascript" src="../token/scripts.js"></script>
   