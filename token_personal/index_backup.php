<?php require('autenticar.php') ;?>

<!doctype html>
<html lang="es">
  
  <?php include '../token/head.php';?>
  <script>
  function ocultar_DNI() {
	  document.f1.inputDni.value = 0
	  /*$('#inputDni').hide();*/
  }
  
  </script>
  
  
  <body class="text-center">
	
	<div class="container offline d-none">
		<header class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
    			<div class="alert alert-warning  none">Se requiere conexión a internet</div>
    		</div>
    	</header>
    </div>


   <form id="f1" class="form-signin online" method="POST" style="opacity: 0;">
   	  
      <input type="hidden" id="inputApp_id" name="app_id" value="" class="form-control">

      <p class="text-center"><img id="logo" src="../token/logo.png"  alt="Logo Colegio" style="width: 80%;"></p>

      <div class="row">
    		<div class="col-12 mb-3 text-left">
            <?php 
            if ( isset($_GET['mensaje']) && strlen(trim($_GET['mensaje'])) ){
            ?>
              <p style="color:red;text-align:center;"><?php echo urldecode($_GET['mensaje']); ?></p>
            <?php } ?>

    	      <label for="inputDni" class="sr-label labelDNI">DNI</label>
    	      <input type="number" id="inputDni" name="login_dni" class="form-control" size="8" minlength="0" maxlength="8"  placeholder="" required autofocus>
    		</div>
	   </div>
	  
	    <div class="row">
  	   	<div class="col-12 mb-3 text-left">
  	      <label for="inputPassword" class="sr-label">CONTRASEÑA</label>
  	      <input type="password" id="inputPassword" name="login_pass" class="form-control" placeholder="" required>
  	    </div>
      </div>
     
      <button class="btn btn-lg btn-success btn-block" type="submit">INICIAR SESIÓN</button>
      <br>
      
      <p class="mt-5 mb-3 text-muted">&copy; Colegio Bertrand Russell <?php echo date("Y") ?></p>
        
    </form>
 
    <?php include '../token/footer.php';?>
    

</body>
</html>