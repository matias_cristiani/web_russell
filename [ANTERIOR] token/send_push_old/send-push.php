<?php 
#version colegio
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);

require_once("medoo.php");
require_once("config.php");
$db = new medoo($odbc_nombre);

$cantidad_de_push = 60;

function enviarPush($app_id, $rest_api_key, $title, $message, $dni, $codigo){
    echo('CACHE OFF: '.rand().'<br>');

    $app = false;
    $extraParams = array();
    $isTest = false;
    $debugear = false;
    
    
    if ($debugear){
        echo "<pre>";
    }

    $heading = array(
        "en" => $title
    );

    $content = array(
        "en" => $message
    );
    
    echo('<br>');
    echo('SEND TO:'.$app_id);
    echo(' | KEY:'.$rest_api_key);

    $fields = array(
        'app_id' => $app_id,
        'data' => array('codigo' => $codigo),
        'contents' => $content,
        'headings' => $heading
    );

   
    $fields['filters'] = array(
                array("field" => "tag", "key" => "dni", "relation" => "=", "value" => $dni)
    );

    // $fields['included_segments'] = ['Active Users','Inactive Users'];
    
    $fields = json_encode($fields);
        
        if ($debugear){
            print_r("\nJSON sent:\n");
            print_r($fields);
        }
         
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $rest_api_key
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         
        $response = curl_exec($ch);
        curl_close($ch);
         
         
        $return["allresponses"] = $response;
        $return = json_encode($return);
         
        if ($debugear){
            print_r("\n\nJSON received:\n");
            print_r($return);
            print_r("\n");
            echo "</pre>";
        }

    return $response;
}



$query = "SELECT TOP ".$cantidad_de_push." * FROM tblNotificacionesPush WHERE enviado = 1";
$mensajes = $db->query($query)->fetchAll();

$query2 = "SELECT Id FROM tblNotificacionesPush WHERE enviado = 0";
$mensajesContar = $db->query($query2)->fetchAll();

?>

<!DOCTYPE html>
<html>
<head>
    <title>Enviar mensajes push</title>
    
<SCRIPT LANGUAGE="JavaScript"> 
function cerrar(){ 
var ventana = window.self;
ventana.opener = window.self;
ventana.close(); 
} 
</script>  
</head>
<body>
<?php

echo "<pre>";
echo "Cantidad de mensajes push pendientes de enviar: ".count($mensajesContar)."\n";

// DEMO
$demo = false;

if ($demo) {

    echo "APP PERSONAL";
    echo "<br>";
    echo "DEMO ACTIVADA: 27822657";
    echo "<br>";
    enviarPush('61a8be03-a90e-4a1e-8d38-a11ecd74de8d', 'OWNmY2IyYzktYTllZS00ZWYyLWI4NDMtZmRmMzk4MWEzOTE2', 'TITULO', 'MENSAJE', '27822657', '1550857101');
    
    die();
}

foreach ($mensajes AS $msj) {
    echo "Enviando push a DNI ".$msj['destinatario_dni']."\n";

    if ( strlen(trim($msj['titulo'])) && strlen(trim($msj['mensaje'])) && strlen(trim($msj['destinatario_dni'])) == 8 ){
        
        $origen = utf8_encode(strtoupper((string)$msj['origen'])) ? utf8_encode(strtoupper((string)$msj['origen'])) : 'ERROR';
        $titulo = utf8_encode((string)$msj['titulo']) ? utf8_encode((string)$msj['titulo']) : 'SIN TITULO';
        $mensaje = utf8_encode((string)$msj['mensaje']) ? utf8_encode((string)$msj['mensaje']) : 'SIN DESCRIPCIÓN';
        $dni = utf8_encode((string)$msj['destinatario_dni']) ? utf8_encode((string)$msj['destinatario_dni']) : '0';
        $codigo = utf8_encode((string)$msj['clave_notificacion']) ? utf8_encode((string)$msj['clave_notificacion']) : '0';

        switch ($origen) {
            case 'P':
                echo "APP Personal"."\n";
                $api_key = "61a8be03-a90e-4a1e-8d38-a11ecd74de8d";
                $rest_api_key = "OWNmY2IyYzktYTllZS00ZWYyLWI4NDMtZmRmMzk4MWEzOTE2";

                $notif = enviarPush( $api_key, $rest_api_key, $titulo, $mensaje, $dni, $codigo );
                $notif = json_decode($notif);
            
                if ( is_array($notif) && isset($notif['allresponses']) ){ 
                    $db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = 'ENVIADO OK' WHERE id = ".$msj['Id'] );
                    sleep(1);
                } else {
                    $db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = '".json_encode($notif)."' WHERE id = ".$msj['Id'] );
                }

                break;
            
            case 'A':
                echo "APP Alumnos"."\n";
                $api_key = "b278c8c6-3615-4bd0-a4bf-6f8e6b1bd017";
                $rest_api_key = "MGI2ZjMyMTktZTEyZS00NWZjLWE3OGYtMDcyNDhhMWEwYTUz";

                $notif = enviarPush( $api_key, $rest_api_key, $titulo, $mensaje, $dni, $codigo );
                $notif = json_decode($notif);
            
                if ( is_array($notif) && isset($notif['allresponses']) ){ 
                    $db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = 'ENVIADO OK' WHERE id = ".$msj['Id'] );
                    sleep(1);
                } else {
                    $db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = '".json_encode($notif)."' WHERE id = ".$msj['Id'] );
                }

                break;
            

            case 'F':
                echo "APP Familia"."\n";
                $api_key = "c6b47334-f16f-4e31-bcf7-9c6a147b1099";
                $rest_api_key = "ZGVmMThmMzctYjA1YS00NzQ3LTlmNDctNzU0OGViZmE0ZmVi";

                $notif = enviarPush( $api_key, $rest_api_key, $titulo, $mensaje, $dni, $codigo );
                $notif = json_decode($notif);
            
                if ( is_array($notif) && isset($notif['allresponses']) ){ 
                    $db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = 'ENVIADO OK' WHERE id = ".$msj['Id'] );
                    sleep(1);
                } else {
                    $db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = '".json_encode($notif)."' WHERE id = ".$msj['Id'] );
                }

                break;
            
            default:
                echo "Se requiere que defina el origen de la notificación"."\n";
                break;
        }


        
    } else {
        echo "Se requiere titulo, mensaje y DNI\n";
    }
}

    echo "</pre>";
    echo "<pre>Versi&oacute;n 2019.03.07 15.00</pre>";
?>

<?php
if ( isset($mensajes) && is_array($mensajes) && count($mensajes) >= $cantidad_de_push ){
?>
<P>Aguarde mientras se envian los mensajes pendientes...</P>
<h4>No cierre esta ventana, hasta que se termine de completar el envío.</h4>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>

<script type="text/javascript">
$(document).ready(function(){
    setTimeout(function(){
        window.location.reload(true);
    }, 5000); // Esperar 5 segundos y refrescar
});
</script>
<?php
} else {
?>
<h4>Todos los mensajes han sido enviados.</h4>
<script language="javascript"> 
    /* cerrar(); */
</script> 
<?php
}
?>

</body>
</html>