<?php 
require_once("medoo.php");
require_once("config.php");
$db = new medoo($odbc_nombre);


function enviarPush($title, $message, $dni)
{
	
	$app = false;
	$extraParams = array();
	$isTest = false;
	$debugear = false;
	
    if ($debugear){
        echo "<pre>";
    }

    $app_id = "61a8be03-a90e-4a1e-8d38-a11ecd74de8d";
    $rest_api_key = "OWNmY2IyYzktYTllZS00ZWYyLWI4NDMtZmRmMzk4MWEzOTE2";
    
    $heading = array(
        "en" => $title
    );

    $content = array(
        "en" => $message
    );

    $fields = array(
        'app_id' => $app_id,
        'data' => array('msj' => ''),
        'contents' => $content,
        'headings' => $heading
    );
     

    $fields['filters'] = array(
                array("field" => "tag", "key" => "dni", "relation" => "=", "value" => $dni)
	);

    
    
    // $fields['included_segments'] = ['Active Users','Inactive Users'];
    

    $fields = json_encode($fields);
    if ($debugear){
        print_r("\nJSON sent:\n");
        print_r($fields);
    }
     
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic ' . $rest_api_key
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     
    $response = curl_exec($ch);
    curl_close($ch);
     
     
    $return["allresponses"] = $response;
    $return = json_encode($return);
     
    if ($debugear){
        print_r("\n\nJSON received:\n");
        print_r($return);
        print_r("\n");
        echo "</pre>";
    }

    return $response;
}

$notif = [];
$notif[] = enviarPush('Enviado '.date("Y-m-d H:i:s"), 'Mensaje con timestamp '.time(), '07594359');
$notif[] = enviarPush('Enviado '.date("Y-m-d H:i:s"), 'Mensaje con timestamp '.time(), '27822657');

print_r($notif);

echo "Version 001";
exit();

/*
$query = "SELECT TOP 60 * FROM tblNotificacionesPush WHERE enviado = 0";
$mensajes = $db->query($query)->fetchAll();

$query2 = "SELECT Id FROM tblNotificacionesPush WHERE enviado = 0";
$mensajesContar = $db->query($query2)->fetchAll();

echo "<pre>";
echo "Cantidad de mensajes push pendientes de enviar: ".count($mensajesContar)."\n";
foreach ($mensajes AS $msj) {
	echo "Enviando push a DNI ".$msj['destinatario_dni']."\n";
	if ( strlen(trim($msj['titulo'])) && strlen(trim($msj['mensaje'])) && strlen(trim($msj['destinatario_dni'])) == 8 ){
		$notif = enviarPush($msj['titulo'], $msj['mensaje'], $msj['destinatario_dni']);
		$notif = json_decode($notif);
		if ( is_array($notif) && isset($notif['allresponses']) ){ 
			$db->query("UPDATE tblNotificacionesPush SET enviado = -1 WHERE id = ".$msj['Id'] );
			sleep(1);
		} else {
			$db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = '".json_encode($notif)."' WHERE id = ".$msj['Id'] );
		}
	} else {
		echo "Se requiere titulo, mensaje y DNI\n";
	}
}

if ( count($mensajesContar) > 0 ) {
	echo "Debe refrescar para continuar enviando los mensajes pendientes\n";
}
*/
echo "</pre>";