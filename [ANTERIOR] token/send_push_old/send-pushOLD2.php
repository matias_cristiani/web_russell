<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require_once("medoo.php");
require_once("config.php");
$db = new medoo($odbc_nombre);

$cantidad_de_push = 60;

function quitarPushViejos($horas_antiguedad = 48 )
{
	global $db;
	$query = "DELETE * FROM tblNotificacionesPush WHERE (((tblNotificacionesPush.enviado)=True) AND ((DateDiff('h',[fecha_hora],Now()))>=".$horas_antiguedad."));";
	$msj_antiguos = $db->query($query);
	
}



function enviarPush($title, $message, $dni)
{
	
	$app = false;
	$extraParams = array();
	$isTest = false;
	$debugear = false;
	
    if ($debugear){
        echo "<pre>";
    }

    $app_id = "61a8be03-a90e-4a1e-8d38-a11ecd74de8d";
    $rest_api_key = "OWNmY2IyYzktYTllZS00ZWYyLWI4NDMtZmRmMzk4MWEzOTE2";
    
    $heading = array(
        "en" => $title
    );

    $content = array(
        "en" => $message
    );

    $fields = array(
        'app_id' => $app_id,
        'data' => array('msj' => ''),
        'contents' => $content,
        'headings' => $heading
    );
     

    $fields['filters'] = array(
                array("field" => "tag", "key" => "dni", "relation" => "=", "value" => $dni)
	);

    
    
    // $fields['included_segments'] = ['Active Users','Inactive Users'];
    

    $fields = json_encode($fields);
    if ($debugear){
        print_r("\nJSON sent:\n");
        print_r($fields);
    }
     
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic ' . $rest_api_key
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     
    $response = curl_exec($ch);
    curl_close($ch);
     
     
    $return["allresponses"] = $response;
    $return = json_encode($return);
     
    if ($debugear){
        print_r("\n\nJSON received:\n");
        print_r($return);
        print_r("\n");
        echo "</pre>";
    }

    return $response;
}


quitarPushViejos();

$query = "SELECT TOP ".$cantidad_de_push." * FROM tblNotificacionesPush WHERE enviado = 0";
$mensajes = $db->query($query)->fetchAll();

$query2 = "SELECT Id FROM tblNotificacionesPush WHERE enviado = 0";
$mensajesContar = $db->query($query2)->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Enviar mensajes push</title>
</head>
<body>
<?php
echo "<pre>";
echo "Cantidad de mensajes push pendientes de enviar: ".count($mensajesContar)."\n";
foreach ($mensajes AS $msj) {
	echo "Enviando push a DNI ".$msj['destinatario_dni']."\n";
	if ( strlen(trim($msj['titulo'])) && strlen(trim($msj['mensaje'])) && strlen(trim($msj['destinatario_dni'])) == 8 ){
		$notif = enviarPush(utf8_encode((string)$msj['titulo']), utf8_encode((string)$msj['mensaje']), utf8_encode((string)$msj['destinatario_dni']));
		$notif = json_decode($notif);
		if ( is_array($notif) && isset($notif['allresponses']) ){ 
			$db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = 'ENVIADO OK' WHERE id = ".$msj['Id'] );
			sleep(1);
		} else {
			$db->query("UPDATE tblNotificacionesPush SET enviado = -1, error_log = '".json_encode($notif)."' WHERE id = ".$msj['Id'] );
		}
	} else {
		echo "Se requiere titulo, mensaje y DNI\n";
	}
}

//if ( count($mensajesContar) > 0 ) {
//	echo "Debe refrescar para continuar enviando los mensajes pendientes\n";
//}

echo "</pre>";
echo "<pre>Versi&oacute;n 20181121 1358</pre>";
?>

<?php
if ( isset($mensajes) && is_array($mensajes) && count($mensajes) >= $cantidad_de_push ){
?>
<P>Aguarde mientras se envian los mensajes pendientes...</P>
<h4>No cierre esta ventana, hasta que se termine de completar el envío.</h4>
<script
  src="lib/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>

<script type="text/javascript">
$(document).ready(function(){
	setTimeout(function(){
		window.location.reload(true);
	}, 5000); // Esperar 5 segundos y refrescar
});
</script>
<?php
} else {
?>
<h4>Todos los mensajes han sido enviados.</h4>
<?php
}
?>

</body>
</html>
























