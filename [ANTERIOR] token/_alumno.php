<?php
session_start();
date_default_timezone_set('America/Argentina/Buenos_Aires');

if (!isset($_POST['login_dni']) || !is_numeric($_POST['login_dni']) || !isset($_POST['login_pass']) || strlen(trim($_POST['login_pass'])) == 0 ) {

	header('Content-Type: text/html; charset=utf-8');
	die("ERROR");
	exit();

} else {

	$dni = trim($_POST['login_dni']);
	$dni = str_replace(".", "", $dni);
	$pass = trim($_POST['login_pass']);

}

require_once("medoo.php");
require_once("config.php");
$db	= new medoo($odbc_nombre);



$usuarios = $db->query($query_login)->fetchAll();

if ( count($usuarios) == 1 ) {
	$_SESSION["KT_Username"] = $dni;
	$_SESSION["contra"]= $pass;
	$url_autenticado = "https://www.colegiorussell.edu.ar/bralumnos/datos_alu.php";
	header('Location: '.$url_autenticado);
	exit();
} else {
	header('Content-Type: text/html; charset=utf-8');
	die("ERROR");
}

?>