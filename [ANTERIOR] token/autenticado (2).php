<?php
session_start();

//TimeZone
date_default_timezone_set('America/Argentina/Buenos_Aires');

//HTTP-ORIGIN
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	// header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

//Datos
$_SESSION["titulo"] = 'Colegio Bertrand Russell - Inicio';

if ( isset($_POST['login_dni']) && strlen(trim($_POST['login_dni'])) > 0 ) {

//Parametros
$_SESSION["login_app"] = 1;

if ($_POST['app_id']) {
		
		$_SESSION["app_id"] = $_POST['app_id']; 	
	 	
	 	switch ($_POST['app_id']) {
	 		
	 		case 'ar.edu.colegiorussell.v2.familia':
				require('autentica_padres.php');  
		    break;
		    
		    case 'ar.edu.colegiorussell.v2.alumnos':
				require('autentica_alumnos.php');
			break;

		    case 'ar.edu.colegiorussell.v2.personal':
				require('autentica_profesores.php');
			break;
				
		    case 'ar.edu.colegiorussell.padre':
				require('autentica_padres.php');	  
		   	break;
		    
		    case 'ar.edu.colegiorussell.alumnos':
				require('autentica_alumnos.php');
			break;

		    case 'ar.edu.colegiorussell.personal':
				require('autentica_profesores.php');
			break;
			
		}
}	

};

?>
