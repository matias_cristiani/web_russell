<?php

if (!isset($_POST['login_dni']) || !is_numeric($_POST['login_dni']) || !isset($_POST['login_pass']) || strlen(trim($_POST['login_pass'])) == 0 ) {

	header('Content-Type: text/html; charset=utf-8');
	die("ERROR");
	exit();

} else {

	$dni = trim($_POST['login_dni']);
	$dni = str_replace(".", "", $dni);
	$pass = trim($_POST['login_pass']);

}

require_once("medoo.php");
require_once("config.php");
$db	= new medoo($odbc_nombre);



$usuarios = $db->query($query_login_personal)->fetchAll();

if ( count($usuarios) == 1 ) {
	$_SESSION["KT_Username"] = $dni;
	$_SESSION["esMiembroDelPersonal"] = true;
	$_SESSION["directivo"] = $usuarios[0]['DirectivoNivel'];
	$_SESSION["contra"]= $pass;	
	
	// $url_autenticado = "https://www.colegiorussell.edu.ar/brprofes/datos_pro.php";
	$url_autenticado = "https://www.colegiorussell.edu.ar/app/token/datos_cargando.php"; 
	
	$mensaje = $_POST['login_dni']; 
	$mensaje2 = $_POST['login_pass'];
	$param3 = $_POST['app_id'];


	header("Location: ".$url_autenticado."?logindni=".urlencode($mensaje)."&loginpass=".urlencode($mensaje2)."&app_id=".urlencode($param3)); 
	exit();
} else {
	//Parametro fuera de sesion
	$_SESSION["login_app"] = "";	
	$mensaje = "DNI o Contraseña incorrectos, por favor intente nuevamente";
    $redireccionar = "https://www.colegiorussell.edu.ar/app/token/index.php?mensaje=".$mensaje;
    header("Location: ".$redireccionar );
}