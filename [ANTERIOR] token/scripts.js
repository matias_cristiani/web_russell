var oWebViewInterface = window.nsWebViewInterface;

(function(){
    var loginUser = 0;
    var loginPass = 0;

    var url_ = window.location.href;
    
    var url = new URL(url_)
    var dni_ = url.searchParams.get("logindni");
    var pass_ = url.searchParams.get("loginpass");
    
    if (dni_ && Number(dni_) > 0) {
      loginUser = dni_
      loginPass = pass_
    }

    /**
     * Cerrar Sessión y des-registrar push
     */
     $('#cerrarSesion').on( "click", function() {
        appLogout();
        window.location = 'https://www.colegiorussell.edu.ar/app/token/';
    });
       
    /**
     * Registers handlers: offline / online mode .
     */
    var $online = $('.online');
    var $offline = $('.offline');

    Offline.on('confirmed-down', function () {
                    
        $online.fadeOut(function () {
            $offline.fadeIn();
        });

        if ($('.btn')) {
            $('.btn').attr("disabled", "disabled");
        }

    });

    Offline.on('confirmed-up', function () {
                    
                    $offline.fadeOut(function () {
                        $online.fadeIn();
                    });

                    if ($('.btn')) {
                        $('.btn').removeAttr("disabled");
                    }
    });
    
    /**
     * Registers handlers for native events.
     */

    
    
    function addNativeMsgListener() {
        /*
        oWebViewInterface.on('loadLanguages', function (arrLanguages) {
            for (var i = 0; i < arrLanguages.length; i++) {
                addLanguageOption(arrLanguages[i]);
            }
        });
        */
    }
    
    /**
     * Defines global functions which will be called from android/ios
     */
    function defineNativeInterfaceFunctions(){

        
        window.iniciar_sesion = function(usuario){
            
            console.log(usuario);
            $('#inputDni').val(usuario.dni);
            $('#inputPassword').val(usuario.password);
			$("#inputDni").hide();
            /*$('#autenticaId').attr('action','autenticado.php');
			$('#inputDni').attr('type','hidden');
			
			
			/*
			$('#inputPassword').attr('type','hidden');
			$('#boton').css('display','none');
			$('#cargando').css('display','block');
			*/
            if (usuario.dni && usuario.password) {
			
               $('.form-signin').submit();    
            }

            setTimeout(function(){
                 $('.online').css({
                            'opacity': 1, 
                            'transition': '1s'
                            });
            }, 1000);

            return new Promise(function(resolve) {
                    resolve('OK');
                    // resolve(false);
            });
        };


         window.app_id = function(app_id){
            console.log('app_id');
            console.log(app_id);
            
            if (app_id) {
                $('#inputApp_id').val(app_id);

                var dni_etiqueta = "D.N.I.";
                
                //familia
                if ( app_id == "ar.edu.colegiorussell.padre" ) {
                    dni_etiqueta = "D.N.I. FAMILIA";
                }
                
                if ( app_id == "ar.edu.colegiorussell.v2.familia" ) {
                    dni_etiqueta = "D.N.I. FAMILIA";
                }
                
                //alumnos
                if ( app_id == "ar.edu.colegiorussell.alumnos" ) {
                    dni_etiqueta = "D.N.I. ALUMNO";
                }
                if ( app_id == "ar.edu.colegiorussell.v2.alumnos" ) {
                    dni_etiqueta = "D.N.I. ALUMNO";
                }
                
                
                //personal
                if ( app_id == "ar.edu.colegiorussell.personal" ) {
                    dni_etiqueta = "D.N.I. PERSONAL";
                    //$("*.badge-personal").removeClass("hide");
                    //$("*.badge-personal").addClass("show");
                }
                
                if ( app_id == "ar.edu.colegiorussell.v2.personal" ) {
                    dni_etiqueta = "D.N.I. PERSONAL";
                    //$("*.badge-personal").removeClass("hide");
                    //$("*.badge-personal").addClass("show");
                }

                $('label.labelDNI').text(dni_etiqueta);
            }
            return new Promise(function(resolve) {
                    resolve('APP_ID SETEADO');
            });
        };
        
    }

    function sendUser(usuario){
        oWebViewInterface.emit('listenUser', usuario);
    }

    function appLogout(){
        oWebViewInterface.emit('listenClear');   
    }
    
     function sendDemo(usuario){
        oWebViewInterface.emit('anyEvent', 'usuario');
    }
    

    /**
     * Defines init webview interface
     */
    function init() {
        
        defineNativeInterfaceFunctions();
        
        setTimeout(function(){
            
            //limitar-dni
            $("#inputDni").keyup(function() {
                var maxChars = 8;
                if ($(this).val().length > maxChars) {
                    $(this).val($(this).val().substr(0, maxChars));
                }
            });

            
            //mostrar-online
            $('.online').css({ 'opacity': 1,  'transition': '1s' });
        

            if ( loginUser && loginUser > 0 ){
                
                setTimeout(function(){
                    sendUser({dni: loginUser, password: loginPass});
                }, 1000);
            
                
            }else{
                console.log(loginUser);
            }

        }, 400);
    
        
        
    }







    /**
     * init webview
     */
    init();

})();