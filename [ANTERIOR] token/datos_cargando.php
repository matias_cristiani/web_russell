<?php require('autenticar.php') ;?>

<!doctype html>
<html lang="es">
  
  <?php include 'head.php';?>
  <?php 
  switch ($_GET['app_id']) {
    
    	case 'ar.edu.colegiorussell.v2.familia':
        $redireccionar = "https://www.colegiorussell.edu.ar/brpadres/datos_pad.php";
		    break;
		    
		    case 'ar.edu.colegiorussell.v2.alumnos':
        $redireccionar = "https://www.colegiorussell.edu.ar/bralumnos/datos_alu.php";
			break;

		    case 'ar.edu.colegiorussell.v2.personal':
        $redireccionar = "https://www.colegiorussell.edu.ar/brprofes/datos_pro.php";
			break;
			
      case 'ar.edu.colegiorussell.padre':
        $redireccionar = "https://www.colegiorussell.edu.ar/brpadres/datos_pad.php";
        break;
      
      case 'ar.edu.colegiorussell.alumnos':
        $redireccionar = "https://www.colegiorussell.edu.ar/bralumnos/datos_alu.php";
        break;

      case 'ar.edu.colegiorussell.personal':
        $redireccionar = "https://www.colegiorussell.edu.ar/brprofes/datos_pro.php";
        break;
    
  }
  ?>
  <meta http-equiv="refresh" content="4;URL='<?php echo $redireccionar; ?>'" /> 
  
  <body class="text-center">
	
	<div class="container offline d-none">
		<header class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
    			<div class="alert alert-warning  none">Se require conexión a internet</div>
    		</div>
    	</header>
    </div>

    <div class="container container-flex">
        <div class="row ">
          <div class="col-md-12">
            <div class="loader"></div>
          </div>
        </div>
        <div class="row ">
          <div class="col-md-12">
              <p>Cargando...</p>
              <!-- <p><a class="btn btn-secondary" id="cerrarSesion" role="button">CERRAR SESIÓN</a></p> -->
          </div>
        </div>

        <hr>

        <footer>
          <p class="mt-5 mb-3 text-muted">&copy; Colegio Bertrand Russell <?php echo date("Y") ?></p>
        </footer>

      </div>


    <?php include 'footer.php';?>
    

</body>
</html>