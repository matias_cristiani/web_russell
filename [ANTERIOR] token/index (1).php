<?php require('autenticar.php') ;?>
<?php require('autenticado.php') ;?>

<!doctype html>
<html lang="es">
  
  <?php include 'head.php';
  if (isset($_GET['login_dni']))
  $_SESSION["login_app"]=1;
  
  ?>
  
  
  <body class="text-center">
	
	<div class="container offline d-none">
		<header class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
    			<div class="alert alert-warning  none">Se requiere conexión a internet</div>
    		</div>
    	</header>
    </div>

        <?php 
		  if ($_SESSION["login_app"]!=1) {
       ?>
   <form action="autenticar.php" class="form-signin online" method="POST" style="opacity: 0;">
            <?php 
		  } else {
      ?>
    <form action="autenticado.php" class="form-signin online" method="POST" style="opacity: 0;">
      <?php 
		  } 
      ?>
  	  
      <input type="hidden" id="inputApp_id" name="app_id" value="" class="form-control">

      <!--<h1 class="h3 mb-3 font-weight-normal titulo">BIENVENIDOS</h1> -->
      <p class="text-center"><img id="logo" src="logo.png" alt="Logo Colegio" style="width: 80%;"></p>
      <div class="row">
		<div class="col-12 mb-3 text-left">
        <?php 
        if ( isset($_GET['mensaje']) && strlen(trim($_GET['mensaje'])) ){
        ?>
          <p style="color:red;text-align:center;"><?php echo urldecode($_GET['mensaje']); ?></p>
        <?php } ?>

        <?php 
		//$vars = get_defined_vars();  
      	// print_r($vars);
		 //print_r($_POST);
		 //sleep (10);
		  if ($_SESSION["login_app"]!=1) {
       ?>
       	      <label for="inputDni" class="sr-label labelDNI">DNI</label>
	      <input type="number" id="inputDni" name="login_dni" class="form-control" size="8" minlength="8" maxlength="8"  placeholder="" required autofocus>
		</div>
	  </div>
	  
	  <div class="row">
	  	<div class="col-12 mb-3 text-left">
	      <label for="inputPassword" class="sr-label">CONTRASEÑA</label>
	      <input type="password" id="inputPassword" name="login_pass" class="form-control" placeholder="" required>
	    </div>
  	  </div>
            <?php 
		  } else {
      ?>
	      
	      <input type="hidden" id="inputDni" name="login_dni" class="form-control" size="8" minlength="8" maxlength="8"  placeholder="" required autofocus>
		</div>
	  </div>
	  
	  <div class="row">
	  	<div class="col-12 mb-3 text-left">
	      
	      <input type="hidden" id="inputPassword" name="login_pass" class="form-control" placeholder="" required>
	    </div>
  	  </div>
            <?php 
		  }
       
		  if ($_SESSION["login_app"]!=1) {
       ?>
   
      		<button class="btn btn-lg btn-success btn-block" type="submit">INICIAR SESIÓN</button>
      		<br>
            <?php 
		  } else {
     		 ?>
      		<p style="color:green;text-align:center;font-weight:bold"><?php echo "CARGANDO"; ?></p>

            <?php }
      		?>
      
      <!--
       <a href="http://www.colbertrandrussell.com.ar/" class="badge-personal badge badge-success hide">Visitar Colegio </a>
       <a href="https://www.colegiorussell.edu.ar/admisiones/inscripcion.php" class="badge-personal badge badge-light hide">Inscribirse en el colegio</a>
      -->

   
   <p class="mt-5 mb-3 text-muted">&copy; Colegio Bertrand Russell <?php echo date("Y") ?></p>
        
    </form>

    <!-- <pre>
      <?php 
      // echo "GET:";
      // print_r($_GET);

      // echo "POST:";
      // print_r($_POST);
      ?>
    </pre> -->

    <?php include 'footer.php';?>
    

 <!-- Optional JavaScript -->
    <script type="text/javascript" src="../token2/lib/es6-promise.min.js"></script>
    <script type="text/javascript" src="../token2/lib/nativescript-webview-interface.js"></script>
    <script type="text/javascript" src="../token2/offline/offline.min.js"></script>
    <script type="text/javascript" src="../token2/scripts.js"></script>
  
</body>
</html>