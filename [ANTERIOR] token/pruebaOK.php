<?php
//Connection statement
require_once('../../brprofes/Connections/BaseLocal.php');

//Aditional Functions
require_once('../../brprofes/includes/functions.inc.php');

session_start();
//TimeZone
date_default_timezone_set('America/Argentina/Buenos_Aires');

//HTTP-ORIGIN
//if (isset($_SERVER['HTTP_ORIGIN'])) {
//	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
//	header('Access-Control-Allow-Credentials: true');
	// header('Access-Control-Max-Age: 86400');    // cache for 1 day
//}

// Access-Control headers are received during OPTIONS requests
//if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

//	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
//		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

//	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
//		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

//	exit(0);
//}

//Datos
//$_SESSION["titulo"] = 'Colegio Bertrand Russell - Inicio';

//if ( isset($_POST['login_dni']) && strlen(trim($_POST['login_dni'])) > 0 ) {

//Parametros
$_SESSION["login_app"] = 1;

if ($_POST['app_id']) {
		
		$_SESSION["app_id"] = $_POST['app_id']; 	
	 	
	 	switch ($_POST['app_id']) {
	 		
	 		case $_POST['app_id']== 'ar.edu.colegiorussell.v2.familia' || $_POST['app_id']== 'ar.edu.colegiorussell.padre':
		    
				require_once('../../brpadres/includes/seguridad.php');
				$KT_rsUser_Source = sprintf("SELECT Contra, LegFam FROM qryLogin WHERE NroDoc = %d", GetSQLValueString($_POST['DNI'], "int"));
				$KT_rsUser=$BaseLocal->Execute($KT_rsUser_Source) or DIE($BaseLocal->ErrorMsg());
				
				$CONTRA=$KT_rsUser -> Fields('Contra');
				$FAMILIA=$KT_rsUser -> Fields('LegFam');
				
				if(md5($_POST['contra'])==$CONTRA){
				
					$modoDemo = false;
					autorizarUsuario($_POST['DNI'], $FAMILIA, $modoDemo);
					$_SESSION['contra']=$_POST['contra'];
					KT_redir("../../brpadres/datos_pad.php");	
				}
				else
				{
					echo "no existe";
				}
			break;
//--------------------------------------------------------------------------------------------------		    
		    case $_POST['app_id']==  'ar.edu.colegiorussell.v2.alumnos' || $_POST['app_id']== 'ar.edu.colegiorussell.alumnos':

				//Aditional Functions
				require_once('../../bralumnos/includes/seguridad.php');
				
				$KT_rsUser_Source = "SELECT PassAlumno ";
					$KT_rsUser_Source .= " FROM `Alumnos` WHERE [DNI NUMERO]=" . GetSQLValueString($_POST['DNI'], "text");
				$KT_rsUser=$BaseLocal->Execute($KT_rsUser_Source) or DIE($BaseLocal->ErrorMsg());
				
				$CONTRA=$KT_rsUser -> Fields('PassAlumno');
				if( MD5($_POST['contra']) == $CONTRA){
					$modoDemo = false;
					autorizarUsuario($_POST['DNI'], $modoDemo);
					$_SESSION['contra']=$_POST['contra'];
					KT_redir("../../bralumnos/datos_alu.php");
				}
				else
				{
					echo "no existe";
				}
			break;
//-------------------------------------------------------------------
		    case $_POST['app_id']== 'ar.edu.colegiorussell.v2.personal' || $_POST['app_id']== 'ar.edu.colegiorussell.personal':

				//Aditional Functions
				require_once('../../brprofes/includes/seguridad.php');
				
				
				$KT_rsUser_Source = "SELECT Contra ";
					$KT_rsUser_Source .= " FROM qryLoginPro WHERE NroDoc=" . GetSQLValueString($_POST['DNI'], "text");
				
				$KT_rsUser=$BaseLocal->Execute($KT_rsUser_Source) or DIE($BaseLocal->ErrorMsg());
				$CONTRA=$KT_rsUser -> Fields('Contra');
				
				if( MD5($_POST['contra']) == $CONTRA){
					autorizarUsuario($_POST['DNI'],"");
					$_SESSION['contra']=$_POST['contra'];
					KT_redir("../../brprofes/datos_pro.php");
				}
				else
				{
					echo "no existe";
				}
			break;
			//-------------------------------------------------------------------------------------------------				
		}
}
//};
?>